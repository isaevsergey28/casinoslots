﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SpinSystem : MonoBehaviour
{
    public delegate void OnSpin();

    public static event OnSpin onSpin;
    public void Spin()
    {
        onSpin?.Invoke();
    }

}
