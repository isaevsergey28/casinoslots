﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnInfo : MonoBehaviour
{
   public int ColumnTravelTime { get; set; }
   public SlotBehaviour[] AllSlotsInColumn { get; private set; } 
   public int SlotsCountInColumn { get; private set; }
   private void Start()
   {
      AllSlotsInColumn = transform.GetComponentsInChildren<SlotBehaviour>();
      SlotsCountInColumn = AllSlotsInColumn.Length;
   }

   public float GetSlotSize()
   {
     return AllSlotsInColumn[0].GetSlotSize();
   }
}
