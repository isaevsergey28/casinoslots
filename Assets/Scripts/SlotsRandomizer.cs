﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class SlotsRandomizer : MonoBehaviour
{
    private void Start()
    {
        SpinSystem.onSpin += RandomizeSlots;
    }

    private void RandomizeSlots()
    {
        SlotBehaviour[] slots = GetComponent<ColumnInfo>().AllSlotsInColumn;
        var listForShuffleSlots = CreateListForShuffleSlots(slots);
        foreach (var slot in slots)
        {
            ShuffleSlots(listForShuffleSlots, slot, slots);
        }
    }

    private void ShuffleSlots(List<int> listForShuffleSlots, SlotBehaviour slot, SlotBehaviour[] slots)
    {
        Vector3 tempObjetPos;
        int randomSlotIndex = Random.Range(0, listForShuffleSlots.Count);
        tempObjetPos = slot.gameObject.transform.position;
        slot.gameObject.transform.position = slots[listForShuffleSlots[randomSlotIndex]].gameObject.transform.position;
        slots[listForShuffleSlots[randomSlotIndex]].gameObject.transform.position = tempObjetPos;
        listForShuffleSlots.RemoveAt(randomSlotIndex);
    }

    private List<int> CreateListForShuffleSlots(SlotBehaviour[] slots)
    {
        List<int> listForShuffleSlots = new List<int>();
        for (int i = 0; i < slots.Length; i++)
        {
            listForShuffleSlots.Add(i);
        }
        return listForShuffleSlots;
    }
}
