﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class SlotsSpawner : MonoBehaviour
{
    [Header("Панель вывода слотов.")]
    [SerializeField] private GameObject _slotsScreen;
    [Header("Все доступные спрайты.")]
    [SerializeField] private Sprite[] _allSlotsSprite;
    [Header("Prefab слота с компонентом SpriteRenderer")]
    [SerializeField] private GameObject _slotPrefab;
    [Header("Border prefab")]
    [SerializeField] private GameObject _borderPrefab;
    
    private SpriteRenderer _slotsScreenSpriteRenderer;
    private float _slotSizeY;
    private float _slotSizeX;
    private float _slotSize;
    private Vector3 _columnSpawnPos;
    private Camera _mainCamera;
    private GameInfo _gameInfo;
    private int _slotsHeightNumber;
    private int _slotsWidthNumber;
    private void Awake()
    {
        _mainCamera = Camera.main;
        _gameInfo = GetComponent<GameInfo>();
        _slotsHeightNumber = _gameInfo.GetSlotsHeightNumber();
        _slotsWidthNumber = _gameInfo.GetSlotsWidthNumber();
        _slotsScreenSpriteRenderer = _slotsScreen.GetComponent<SpriteRenderer>();
        FindFistColumnSpawn();
        CalculateSlotSize();
        SpawnColumns();
        
    }
    private void FindFistColumnSpawn()
    {
        _columnSpawnPos =
            new Vector3(_mainCamera.transform.position.x - (_mainCamera.aspect * _mainCamera.orthographicSize),
                _slotsScreenSpriteRenderer.bounds.max.y);
    }
    private void CalculateSlotSize()
    {
        _slotSizeY = _slotsScreenSpriteRenderer.bounds.size.y / _slotsHeightNumber;
        _slotSizeX = (_mainCamera.aspect *  2.0f * _mainCamera.orthographicSize)/ _slotsWidthNumber;
        _slotSize = _slotSizeX <= _slotSizeY ? _slotSizeX : _slotSizeY;
    }
    private void SpawnColumns()
    {
        _columnSpawnPos.x += _slotSizeX / 2;
        for (int i = 0; i < _slotsWidthNumber; i++)
        {
            GameObject column = new GameObject();
            SetColumnInfo(column, i);
            SpawnSlotsInColumn(column);
        }
    }

    private void SetColumnInfo(GameObject column, int i)
    {
        column.AddComponent<ColumnInfo>();
        column.AddComponent<SlotsPositionNormalizer>();
        column.AddComponent<SlotsRandomizer>();
        if (column.TryGetComponent<ColumnInfo>(out ColumnInfo columnInfo))
        {
            columnInfo.ColumnTravelTime = i + 1;
        }

        column.name = "Column";
        column.tag = "Column";
        column.transform.position = _columnSpawnPos;
        _columnSpawnPos.x += _slotSizeX;
        column.transform.parent = this.gameObject.transform;
    }

    private void SpawnSlotsInColumn(GameObject column)
    {
        int spriteIndex = 0;
        float newPosY = column.transform.position.y;
        newPosY -= _slotSizeY / 2;
        for (int i = 0; i < _slotsHeightNumber; i++)
        {
            GameObject slot;
            slot = SetSlotInfo(column, ref newPosY, ref spriteIndex);
            SetSpriteForSlot(slot, ref spriteIndex);
        }
        SpawnOutsideSlots(column);
    }

    private GameObject SetSlotInfo(GameObject column, ref float newPosY, ref int spriteIndex)
    {
        GameObject slot;
        slot = Instantiate(_slotPrefab, column.transform);
        slot.transform.position = new Vector3(column.transform.position.x, newPosY);
        newPosY -= _slotSizeY;
        slot.transform.localScale = new Vector3(_slotSize, _slotSize);
        if (spriteIndex == _allSlotsSprite.Length)
        {
            spriteIndex = 0;
        }

        return slot;
    }

    private void SpawnOutsideSlots(GameObject column)
    {
        GameObject slot;
        int spriteIndex = 0;
        slot = Instantiate(_slotPrefab , column.transform);
        slot.transform.position = new Vector3(column.transform.position.x, column.transform.position.y + _slotSize / 2);
        slot.transform.localScale = new Vector3(_slotSize, _slotSize);
        SetSpriteForSlot(slot, ref spriteIndex);
        
        slot = Instantiate(_slotPrefab , column.transform);
        slot.transform.position = new Vector3(column.transform.position.x, column.transform.position.y - (_slotSize * _slotsHeightNumber ) - _slotSize / 2);
        slot.transform.localScale = new Vector3(_slotSize, _slotSize);
        SetSpriteForSlot(slot, ref spriteIndex);
        
        SpawnBorder(column);
    }

    private void SetSpriteForSlot(GameObject slot, ref int spriteIndex)
    {
        if(slot.TryGetComponent<SpriteRenderer>(out SpriteRenderer spriteRenderer))
        {
            spriteRenderer.sprite = _allSlotsSprite[spriteIndex++];
        }
    }

    private void SpawnBorder(GameObject column)
    {
        GameObject border = Instantiate(_borderPrefab, column.transform);
        border.tag = "Border";
        border.name = "Border";
        border.transform.position = 
            new Vector3(column.transform.position.x, column.transform.position.y - (_slotSize * (_slotsHeightNumber + 1 )) - _slotSize / 2);
    }
}
