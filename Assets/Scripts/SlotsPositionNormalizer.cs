﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class SlotsPositionNormalizer : MonoBehaviour
{
    private GameObject _currentMaxSlotInColumn;
    private float _offset;
    private int _stoppedSlots;
    private ColumnInfo _parentColumn;
    
    private void Start()
    {
       _parentColumn = GetComponent<ColumnInfo>();
       _currentMaxSlotInColumn = _parentColumn.AllSlotsInColumn[0].gameObject;
    }
    
    public void CheckSlotsMovement()
       {
          _stoppedSlots++;
          if (_stoppedSlots == _parentColumn.SlotsCountInColumn)
          {
             NormalizeSlots();
             _stoppedSlots = 0;
          }
       }
       private void NormalizeSlots()
       {
          FindFirstSlotInColumn();
          FindOutDifference();
          ChangePosY();
       }
       private void FindFirstSlotInColumn()
       {
          foreach (var slot in _parentColumn.AllSlotsInColumn)
          {
             if (slot.gameObject.transform.position.y >= _currentMaxSlotInColumn.transform.position.y)
             {
                _currentMaxSlotInColumn = slot.gameObject;
             }
          }
       }
    
       private void FindOutDifference()
       {
          _offset = _currentMaxSlotInColumn.transform.position.y - this.transform.position.y + _parentColumn.GetSlotSize() / 2;
       }
       private void ChangePosY()
       {
          foreach (var slot in _parentColumn.AllSlotsInColumn)
          {
             slot.transform.position = new Vector3(slot.transform.position.x, slot.transform.position.y - _offset);
          }
       }
}
