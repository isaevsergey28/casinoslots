﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfo : MonoBehaviour
{
    [Header("Количество слотов в высоту.")]
    [SerializeField] private int _slotsHeightNumber;
    [Header("Количество слотов в ширину.")]
    [SerializeField] private int _slotsWidthNumber;
    public int GetSlotsHeightNumber()
    {
        return _slotsHeightNumber;
    }
    public int GetSlotsWidthNumber()
    {
        return _slotsWidthNumber;
    }
}
