﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotBehaviour : MonoBehaviour
{
    private float _slotSize;
    private int _slotsCount;
    private bool _isMoving = false;
    private float _speedMovement = 25;
    private float _travelTime;
    private bool _isWaitForStop = false;

    private void Start()
    {
         SpinSystem.onSpin += MoveAllSlots;
        _slotSize = transform.localScale.y;
        _slotsCount = transform.parent.GetComponent<ColumnInfo>().SlotsCountInColumn;
        FindOutTravelTime();
    }

    private void Update()
    {
        if (_isMoving)
        {
            transform.position += new Vector3(0, -1) * (_speedMovement * Time.deltaTime);
            StartCoroutine(StopSpinning());
        }
    }

    public float GetSlotSize()
    {
        return _slotSize;
    }
    private IEnumerator StopSpinning()
    {
        if (_isWaitForStop)
        {
            yield break;
        }
        _isWaitForStop = true;
        yield return new WaitForSeconds(_travelTime);
        _isMoving = false;
        if (transform.parent.TryGetComponent<SlotsPositionNormalizer>(out SlotsPositionNormalizer slotsPositionNormalizer))
        {
            slotsPositionNormalizer.CheckSlotsMovement();
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Border"))
        {
            float newPosY = transform.position.y + _slotSize * _slotsCount;
            transform.position = new Vector3(transform.position.x, newPosY);
        }
    }

    private void MoveAllSlots()
    {
        _isMoving = true;
        _isWaitForStop = false;
    }

    private void FindOutTravelTime()
    {
        if (transform.parent.TryGetComponent(out ColumnInfo columnInfo))
        {
            _travelTime = columnInfo.ColumnTravelTime;
        }
    }
    
}
